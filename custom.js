
const { app, autoUpdater, dialog } = require('electron')
require('update-electron-app')({
    repo: ' https://yuro_saroyan@bitbucket.org/yuro_saroyan/electrontest.git',
    updateInterval: '1 hour',
})
const path = require('path');

console.log(autoUpdater);
let btn = document.getElementById("update")
const server = 'https://yuro_saroyan@bitbucket.org/yuro_saroyan/electrontest.git'
const url = `${server}/update/${process.platform}`
autoUpdater.setFeedURL({ url })
btn.addEventListener("click", function (){

    autoUpdater.checkForUpdates()
    autoUpdater.on('update-downloaded', (event, releaseNotes, releaseName) => {
        const dialogOpts = {
            type: 'info',
            buttons: ['Restart', 'Later'],
            title: 'Application Update',
            message: process.platform === 'win32' ? releaseNotes : releaseName,
            detail: 'A new version has been downloaded. Restart the application to apply the updates.'
        }

        dialog.showMessageBox(dialogOpts).then((returnValue) => {
            if (returnValue.response === 0) autoUpdater.quitAndInstall()
        })
    })
    autoUpdater.on('error', message => {
        console.error('There was a problem updating the application')
        console.error(message)
    })
})
